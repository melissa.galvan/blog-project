# Blog



### Présentation du projet 

> Projet individuel réalisé en formation.
> Nous devions créer un blog du thème de notre choix.



## Technologies utilisées 

* HTML5
    * TWIG
* CSS3
    * Bootstrap
* PHP 7 
    * Symfony 4 
* MySQL
    * Workbench



## Intérêt

Utilisation et apprentissage de SYMFONY 4, PHP 7, MYSQL



## Installation / Utilisation

1. __Cloner le projet__.
2. Installer les dépendances avec __composer install__.
3. Créer un fichier .env.local avec dedans DATABASE_URL=mysql://__user__:__password__@127.0.0.1:3306/__db_name__ (en remplaçant user, password et db_name par vos informations de connexion à votre base de données).



## Fonctionnalitées intéressantes

* Il est possible d'ajouter un article, de le modifier et de le supprimer.
* Possibilité d'entrer une url pour charger une image dans un article.
* Une barre de recherche est à disposition des utilisateurs/rices.



## Wireframe et Design actuel 

##### *Wireframe*
<img src="public/images/wireframe-blog.png" style="width:400px;height:500px;">

##### *Design actuel*
<img src="public/images/blog2.png" style="width:700px;height:400px;">



## Exemple de code utilisé

```php
    public function findLastRows(): array
    {
        $posts = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM post order by id_post desc limit 6");

        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            $posts[] = $this->sqlToPost($line);
        }
        return $posts;
    }
```

Sert à afficher les 6 derniers articles(page d'accueil du blog).
